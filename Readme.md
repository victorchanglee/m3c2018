### High Performance Computing
#### Autumn, 2018

#### Announcements (see [course webpage](https://imperialhpsc.bitbucket.io) for further information)

* I've set up a forum on Piazza to collect/discuss software problems. Please sign up [here:](https://piazza.com/imperial.ac.uk/fall2018/m3c) and participate! Posts there will get priority over emails.

* There will be an additional office hour on Monday, 8/10/18, 4-5pm, MLC if you need help with software installation or have general questions about the course

* Links for lecture 1 slides, introductory Python material, and course software installation instructions can all be found on the course webpage.


Codes, slides, exercises and solutions are (or will be) included in the course repo. Syncing your
fork and then updating your clone (using *git pull*) will give you your own copy
of all of these files.
